<?php

namespace App\Http\Controllers;

use App\Models\User;
use App\Models\Wallet;
use App\Models\WithdrawalCode;
use App\Models\WithdrawalLogs;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Log;
use MPESA;

class MpesaController extends Controller
{
    public  function pushwithdrawal(Request $request){
        try {
            $user=User::find($request->user_id);
            if(empty($user)){
                return ['status'=>false,'message'=>'User supplied does not exist'];
            }
            $walletData=Wallet::where('user_id',$request->user_id)->first();
            $wallet=Wallet::find($walletData->id);
            if($wallet->amount<$request->amount){
                return ['status'=>false,'message'=>'You have insufficient amount.'];
            }
            $code=WithdrawalCode::where('code',$request->code)->where('user_id',$request->user_id)->where('status','Active')->first();
            if(empty($code)){
                return ['status'=>false,'message'=>'Invalid OTP transactional code.'];
            }

            if(strlen($user->phone)==10){
                $phone='254'.substr($user->phone,1);
            }else{
                $phone=str_replace(' ','','254'.substr($user->phone,4));
            }
            $mpesa = MPESA::b2c($phone,(int)$request->amount_to_pay,'SalaryPayment', 'No Remarks');
            $codedata=WithdrawalCode::find($code->id);
            $request['status']='Used';
            $codedata->update($request->all());
            
            $request['amount_to_save']=$request->amount;
            $request['amount']=$wallet->amount-$request->amount;
            $wallet->update($request->all());

            $request['system_ref']='ED'.mt_rand(100000,999999).'A';
            $request['amount']=$request->amount;
            $request['amount_to_pay']=$request->amount_to_pay;
            $request['phone']=$phone;
            $request['ConversationID']=$mpesa->ConversationID;
            $request['OriginatorConversationID']=$mpesa->OriginatorConversationID;
            $request['ResponseCode']=$mpesa->ResponseCode;
            $request['ResponseDescription']=$mpesa->ResponseDescription;
            $request['status']='PENDING';
            $request['amount']=$request->amount_to_save;
            $w=WithdrawalLogs::create($request->all());

            Log::info(json_encode($mpesa));
            return ['status'=>true,'data'=>$mpesa,'message'=>'Transaction initiated successfully'];
        } catch (\Exception $e) {
            return ['status'=>false,'message'=>$e->getMessage()];
        }
    }
}
