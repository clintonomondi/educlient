<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="utf-8">
    <meta content="width=device-width, initial-scale=1.0" name="viewport">

    <title>Edubora</title>

    <meta name="description" content="">
    <meta property="og:type" content="website"/>
    <meta property="og:title" content="Boost your Grades"/>
    <meta property="og:image" content="/images/logo3.jpeg"/>
    <meta property="og:description" content="Edubora is an e-learning platform that connects learners with all resources they require to reach their full potential."/>

    {{--<!-- Favicons -->--}}
    <link href="/images/logo3.jpeg" rel="icon">
    {{--<link href="/assets/img/apple-touch-icon.png" rel="apple-touch-icon">--}}

    <!-- Google Fonts -->
    <link href="https://fonts.googleapis.com/css?family=Open+Sans:300,300i,400,400i,600,600i,700,700i|Raleway:300,300i,400,400i,500,500i,600,600i,700,700i|Poppins:300,300i,400,400i,500,500i,600,600i,700,700i" rel="stylesheet">

    <!-- Vendor CSS Files -->
    <link href="/assets/vendor/animate.css/animate.min.css" rel="stylesheet">
    <link href="/assets/vendor/bootstrap/css/bootstrap.min.css" rel="stylesheet">
    <link href="/assets/vendor/bootstrap-icons/bootstrap-icons.css" rel="stylesheet">
    <link href="/assets/vendor/boxicons/css/boxicons.min.css" rel="stylesheet">
    <link href="/assets/vendor/glightbox/css/glightbox.min.css" rel="stylesheet">
    <link href="/assets/vendor/swiper/swiper-bundle.min.css" rel="stylesheet">
    <link href="/assets/css/style.css" rel="stylesheet">
    <link href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css" rel="stylesheet"/>

    <link href="/css/filter.css" rel="stylesheet">
    <link href="/css/invoice.css" rel="stylesheet">

    <script data-ad-client="ca-pub-2885740322818519" async src="https://pagead2.googlesyndication.com/pagead/js/adsbygoogle.js"></script>
</head>

<body >
<div class="content2">
    <div id="app">
        <app></app>
    </div>
    <script src="{{ mix('js/app.js') }}"></script>
</div>
    {{--<div class="container">--}}
        {{--<script async src="https://pagead2.googlesyndication.com/pagead/js/adsbygoogle.js"></script>--}}
        {{--<!-- devmyessay -->--}}
        {{--<ins class="adsbygoogle"--}}
             {{--style="display:block"--}}
             {{--data-ad-client="ca-pub-2885740322818519"--}}
             {{--data-ad-slot="3782448631"--}}
             {{--data-ad-format="auto"--}}
             {{--data-full-width-responsive="true"></ins>--}}
        {{--<script>--}}
            {{--(adsbygoogle = window.adsbygoogle || []).push({});--}}
        {{--</script>--}}
    {{--</div>--}}



    <!-- ======= Footer ======= -->
    <footer id="footer" class="mb-0">

        <div class="footer-top">
            <div class="container">
                <div class="row">

                    <div class="col-lg-3 col-md-6 footer-links">
                        <h4>Useful Links</h4>
                        <ul>
                            <li><i class="bx bx-chevron-right"></i> <a href="#">Home</a></li>
                            <li><i class="bx bx-chevron-right"></i> <a href="#">About us</a></li>
                            <li><i class="bx bx-chevron-right"></i> <a href="#">Shop</a></li>
                            <li><i class="bx bx-chevron-right"></i> <a href="#">Contact us</a></li>
                            <li><i class="bx bx-chevron-right"></i> <a href="#">Privacy policy</a></li>
                        </ul>
                    </div>

                    <div class="col-lg-3 col-md-6 footer-links">
                        <h4>Customer Services</h4>
                        <ul>
                            <li><i class="bx bx-chevron-right"></i> <a href="#">My account</a></li>
                            <li><i class="bx bx-chevron-right"></i> <a href="#">Order history</a></li>
                            <li><i class="bx bx-chevron-right"></i> <a href="#">FAQs</a></li>
                            <li><i class="bx bx-chevron-right"></i> <a href="#">Help center</a></li>
                            <li><i class="bx bx-chevron-right"></i> <a href="#">Specials</a></li>
                        </ul>
                    </div>

                    <div class="col-lg-3 col-md-6 footer-contact">
                        <h4>Contact Us</h4>
                        <p>

                            <strong>Phone:</strong> +254717791504<br>
                            <strong>Email:</strong> <a href="/cdn-cgi/l/email-protection" class="__cf_email__" data-cfemail="e980878f86a98c91888499858cc78a8684">info@edubora.co.ke</a><br>
                        </p>

                    </div>

                    <div class="col-lg-3 col-md-6 footer-info">


                        <div class="d-flex">
                            <img src="/assets/payments/mpesa.png" alt="" class="mr-5">
                            <img src="/assets/payments/1.png" alt="" class="mr-5">
                            <img src="/assets/payments/3.png" alt="" class="mr-5">
                            <img src="/assets/payments/4.png" alt="" class="mr-5">
                        </div><hr>
                        <!-- /.payment-methods -->

                        <div class="social-links mt-3">
                            <a href="https://api.whatsapp.com/send?phone=+254717791504&text=Hello,Edubora, enquiry" class="twitter" target="_blank"><i class="bx bxl-whatsapp"></i></a>
                            <a href="https://twitter.com/Edubora2" class="twitter" target="_blank"><i class="bx bxl-twitter" ></i></a>
                            <a href="https://web.facebook.com/edubora.kcpe.kcse.revision" class="facebook" target="_blank"><i class="bx bxl-facebook"></i></a>
                            <a href="https://www.youtube.com/channel/UCmcaTejXrwbW-LCO8lOzc9w" class="google-plus" target="_blank"><i class="bx bxl-youtube"></i></a>
                            <a href="https://www.linkedin.com/company/edubora/" class="linkedin" target="_blank"><i class="bx bxl-linkedin"></i></a>
                            <a href="https://www.instagram.com/edubora_academy/" class="instagram" target="_blank"><i class="bx bxl-instagram"></i></a>
                        </div>
                    </div>

                </div>
            </div>
        </div>

        <div class="container">
            <div class="copyright">
                &copy; Copyright <strong><span>Edubora</span></strong>. All Rights Reserved
            </div>
        </div>
    </footer><!-- End Footer -->


<script src="https://code.jquery.com/jquery-3.2.1.slim.min.js" ></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.12.9/umd/popper.min.js" ></script>
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/js/bootstrap.min.js"  ></script>

    <a href="#" class="back-to-top d-flex align-items-center justify-content-center"><i class="bi bi-arrow-up-short"></i></a>
    <script src="https://code.jquery.com/jquery-3.0.0.js"></script>
    <!-- Vendor JS Files -->
    <script data-cfasync="false" src="/cdn-cgi/scripts/5c5dd728/cloudflare-static/email-decode.min.js"></script>
    <script src="/assets/vendor/bootstrap/js/bootstrap.bundle.min.js"></script>
    <script src="/assets/vendor/glightbox/js/glightbox.min.js"></script>
    <script src="/assets/vendor/isotope-layout/isotope.pkgd.min.js"></script>
    <script src="/assets/vendor/php-email-form/validate.js"></script>
    <script src="/assets/vendor/purecounter/purecounter.js"></script>
    <script src="/assets/vendor/swiper/swiper-bundle.min.js"></script>
    <script src="/assets/vendor/waypoints/noframework.waypoints.js"></script>

    <!-- Template Main JS File -->
    <script src="/assets/js/main.js"></script>
    <script src="/loader/center-loader.js"></script>
    <script src="/assets/js/style.js"></script>
    <script src="/js/chat.js"></script>

    <script>if( window.self == window.top ) { (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){ (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o), m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m) })(window,document,'script','//www.google-analytics.com/analytics.js','ga'); ga('create', 'UA-55234356-4', 'auto'); ga('send', 'pageview'); } </script>
</body>

</html>
