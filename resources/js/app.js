import Vue from 'vue'
import VueRouter from 'vue-router'
import VueAxios from 'vue-axios';
import axios from 'axios';
import VueTelInput from 'vue-tel-input'
import Notifications from 'vue-notification'
import VueToast from 'vue-toast-notification';
import 'vue-toast-notification/dist/theme-sugar.css';
import JwPagination from 'jw-vue-pagination';
import VueTimeago from 'vue-timeago'

Vue.use(VueRouter);
Vue.use(VueAxios, axios);
Vue.use(VueTelInput);
Vue.use(Notifications);
Vue.use(VueToast);
Vue.component('jw-pagination', JwPagination);
Vue.use(VueTimeago, {
    name: 'Timeago', // Component name, `Timeago` by default
    locale: 'en', // Default locale
});

import App from './view/App'
import Index from './pages/index'
import Login from './pages/login'
import Register from './pages/register'
import Home from './pages/home'
import Contact from './pages/contact'
import Shop from './Books/shop'
import Shop_category from './Books/shopping_category'
import NotFound from './pages/404'
import Upload from './Books/upload'
import Myitems from './Books/myitems'
import Single from './Books/single'
import CheckoutSingle from './Books/checkout_single'
import Shopping from './Books/shopping'
import About from './pages/about'
import Edit from './Books/edit'
import Invoice from './Books/invoice'
import Orders from './Books/orders'
import Search from './Books/search'
import Donwnload from './Books/downloads'
import Forget from './pages/forget'
import Confirm from './pages/confirm'
import Reset from './pages/reset_password'
import Profile from './pages/profile'
import Posts from './Discusion/index'
import Post from './Discusion/post'
import Logs from './pages/logs'
import Account from './pages/account'



const router = new VueRouter({
    mode: 'history',
    routes: [
        {
            path: '/account',
            name: 'account',
            component: Account
        },
        {
            path: '/logs',
            name: 'logs',
            component: Logs
        },
        {
            path: '/',
            name: 'index',
            component: Index
        },
        {
            path: '/profile',
            name: 'profile',
            component: Profile
        },
        {
            path: '/posts',
            name: 'posts',
            component: Posts
        },
        {
            path: '/posts/post',
            name: 'post',
            component: Post
        },
        {
            path: '/password/reset',
            name: 'reset',
            component: Reset
        },
        {
            path: '/forget',
            name: 'forget',
            component: Forget
        },
        {
            path: '/confirm',
            name: 'confirm',
            component: Confirm
        },
        {
            path: '/orders',
            name: 'orders',
            component: Orders
        },
        {
            path: '/downloads',
            name: 'downloads',
            component: Donwnload
        },
        {
            path: '/search',
            name: 'search',
            component: Search
        },
        {
            path: '/login',
            name: 'login',
            component: Login
        },
        {
            path: '/register',
            name: 'register',
            component: Register
        },
        {
            path: '/home',
            name: 'home',
            component: Home
        },
        {
            path: '/contact',
            name: 'contact',
            component: Contact
        },
        {
            path: '/shop/:id',
            name: 'shop',
            component: Shop
        },
        {
            path: '/shop/category/:id',
            name: 'shop_category',
            component: Shop_category
        },
        { path: '/404', component: NotFound },
        { path: '*', redirect: '/404' },

        {
            path: '/upload',
            name: 'upload',
            component: Upload
        },
        {
            path: '/myitems',
            name: 'myitems',
            component: Myitems
        },
        {
            path: '/single/:id',
            name: 'single',
            component: Single
        },
        {
            path: '/checkout/single/:id',
            name: 'checkoutsingle',
            component: CheckoutSingle
        },
        {
            path: '/shopping',
            name: 'shopping',
            component: Shopping
        },
        {
            path: '/edit/:id',
            name: 'edit',
            component: Edit
        },
        {
            path: '/invoice/:id',
            name: 'invoice',
            component: Invoice
        },
    ],
});


const app = new Vue({
    el: '#app',
    components: { App },
    router,

});

